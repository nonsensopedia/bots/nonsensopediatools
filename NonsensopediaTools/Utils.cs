using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools
{
    public static class Constants
    {
        public static readonly int[] ContentNamespaces = { 0, 100, 102, 104, 106, 108, 114 };

        public const int UserTalkNs = 3;

        public static readonly string[] Teksty =
        {
            "W Excelu się nie zgadza :<",
            "Ja mam fajrant, nk naprawi:",
            "Weź ktoś zrób coś:",
            "Weź napraw, samo się nie zrobi:",
            "Coś się, coś się popsuło:"
        };

        public const string NonsaApiEndpoint = "https://nonsa.pl/api.php";

        public static readonly CultureInfo PolishCulture = CultureInfo.GetCultureInfo("pl-PL");
        
        public const string MaintainerUserName = "Ostrzyciel";
    }

    public static class Utils
    {
        public static IEnumerable<string> GenerateSubcategoriesRecursively(WikiSite site, string start)
        {
            foreach (var sub in GetSubcategories(site, start))
            {
                yield return sub;
                foreach (var subsub in GenerateSubcategoriesRecursively(site, sub))
                    yield return subsub;
            }
        }

        public static IEnumerable<string> GetSubcategories(WikiSite site, string cat)
        {
            var catGen = new CategoryMembersGenerator(site, cat)
            {
                NamespaceIds = new [] {14}
            };
            return catGen.EnumItemsAsync().ToEnumerable().Select(x => x.Title);
        }

        public static string NormalizePageTitle(string title)
        {
            title = HttpUtility.UrlDecode(title) ?? "";
            title = title.Trim().Replace("_", " ");

            if (title.Length == 0) return "";

            title = title[0].ToString().ToUpperInvariant() + title[1..];
            return title;
        }
    }
}