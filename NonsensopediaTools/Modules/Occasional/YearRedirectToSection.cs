﻿using System;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Occasional
{
    class YearRedirectToSection : IModule
    {
        private WikiSite _nonsa;
        private Options _options;

        public bool IsToBeRun(Options opt) => opt.YearRedirectToSection;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            _nonsa = nonsa;
            _options = opt;

            DoCentury("XVI wiek", 1501);
            //DoCentury("XVII wiek", 1601);
            //DoCentury("XVIII wiek", 1701);
        }

        private void DoCentury(string target, int startYear)
        {
            for (int year = startYear; year < startYear + 100; year++)
            {
                var page = new WikiPage(_nonsa, year.ToString());
                page.RefreshAsync(PageQueryOptions.FetchContent).Wait();

                var decade = (year - startYear) / 10;
                var newContent = $"#PATRZ [[{target}#{startYear + decade * 10}–{startYear + decade * 10 + 9}]]";
                if (newContent == page.Content) continue;

                page.Content = newContent;
                if (!_options.DryRun)
                    page.UpdateContentAsync("", true, true).Wait();

                Console.WriteLine(page.Title);
            }
        }
    }
}
