using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Periodic
{
    public class KatPortal : IModule
    {
        private const string _katPortalR = @"\{\{katportal[^}]*?\}\}";
        private static readonly Regex KatPortalRegex = new Regex(_katPortalR,
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex KatPortalNewlineRegex = new Regex(_katPortalR + @"\n?",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);

        public bool IsToBeRun(Options opt) => opt.KatPortal;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            int updated = 0, added = 0, removed = 0;
            Console.WriteLine("Zbieram kategorie do zaportalowania...");
            
            var portals = EnumeratePortals();
            var catsToPortals = new Dictionary<string, IEnumerable<string>>(portals
                .SelectMany(x => x.Item2.Select(cat => (cat, portal: x.Item1)))
                .GroupBy(
                    x => x.cat,
                    x => x.portal,
                    (cs, ps) => new KeyValuePair<string, IEnumerable<string>>(
                        new WikiPage(nonsa, cs, 14).Title, ps
                    )
                )
            );
            var recursiveCats = new Dictionary<string, IEnumerable<string>>(catsToPortals);
            foreach (var kvp in catsToPortals)
            foreach (var nestedCat in Utils.GenerateSubcategoriesRecursively(nonsa, kvp.Key))
                if (!recursiveCats.ContainsKey(nestedCat))
                    recursiveCats.Add(nestedCat, kvp.Value);

            Console.WriteLine("Wstawiam szablon...");

            foreach (var (catName, portalTitles) in recursiveCats)
            {
                var catPage = new WikiPage(nonsa, catName);
                var catPageTask = catPage.RefreshAsync(PageQueryOptions.FetchContent);

                var sb = new StringBuilder("{{KatPortal|");
                foreach (var portalTitle in portalTitles)
                    sb.Append(portalTitle.Replace("Portal:", ""));
                sb.Append("}}");
                
                catPageTask.Wait();
                if (!catPage.Exists) continue;

                var katAnmIndex = catPage.Content.IndexOf("{{KatANM", StringComparison.InvariantCultureIgnoreCase);
                var afterKatAnm = katAnmIndex != -1 && katAnmIndex <
                                  catPage.Content.IndexOf("{{KatPortal", StringComparison.InvariantCultureIgnoreCase);
                if (afterKatAnm)
                    catPage.Content = KatPortalNewlineRegex.Replace(catPage.Content, "").Trim();

                if (KatPortalRegex.IsMatch(catPage.Content))
                {
                    catPage.Content = KatPortalRegex.Replace(catPage.Content, sb.ToString());
                    updated++;
                    if (!opt.DryRun)
                        catPage.UpdateContentAsync("Zaktualizowano [[Szablon:KatPortal]]", true, true).Wait();
                }
                else
                {
                    var ix = catPage.Content.IndexOf("{{KatANM", StringComparison.InvariantCultureIgnoreCase);
                    if (ix != -1)
                        catPage.Content = catPage.Content.Insert(ix, $"{sb}\n");
                    else
                    {
                        ix = catPage.Content.LastIndexOf("}}", StringComparison.Ordinal);
                        catPage.Content = ix == -1
                            ? catPage.Content.Insert(0, $"{sb}\n\n")
                            : catPage.Content.Insert(ix + 2, $"\n{sb}");
                    }

                    added++;
                    if (!opt.DryRun) 
                        catPage.UpdateContentAsync("Wstawiono [[Szablon:KatPortal]]", true, true).Wait();
                }
            }
            
            Console.WriteLine("Usuwam zbędne...");

            var linkingGen = new TranscludedInGenerator(nonsa, "Szablon:KatPortal")
            {
                PaginationSize = 5000,
                NamespaceIds = new[] {14}
            };
            
            linkingGen.EnumPagesAsync(PageQueryOptions.FetchContent)
                .Where(p => !recursiveCats.ContainsKey(p.Title))
                .ForEachAsync(cat =>
                    {
                        cat.Content = KatPortalNewlineRegex.Replace(cat.Content, "").Trim();
                        removed++;
                        if (!opt.DryRun) cat.UpdateContentAsync("Usunięto zbędny [[Szablon:KatPortal]]", true, true).Wait();
                    }
                ).Wait();
            
            Console.WriteLine($"KatPortal dodane: {added} zmienione: {updated} usunięte: {removed}");
        }
        
        private IEnumerable<(string, IEnumerable<string>)> EnumeratePortals()
        {
            var param = new Dictionary<string, string>
            {
                { "action", "askargs" },
                { "format", "json" },
                { "api_version", "3" },
                { "conditions", "Portal:+|Zawiera kategorię::+" },
                { "printouts", "Zawiera kategorię" },
                { "parameters", "limit=100" }
            };
            
            var result = Constants.NonsaApiEndpoint
                .SetQueryParams(param)
                .GetStringAsync()
                .Result;
            var json = JObject.Parse(result);
            return json["query"]["results"].Values()
                .Select(portalRow => (
                    (portalRow as JProperty).Name,
                    portalRow.First["printouts"].First.Values().Select(x => (string)x["fulltext"])                        
                ));
        }
    }
}