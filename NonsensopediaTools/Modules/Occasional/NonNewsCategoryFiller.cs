﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Occasional
{
    class NonNewsCategoryFiller : IModule
    {
        private bool _dryRun;
        private WikiSite _nonsa;

        private static readonly string[] Months =
        {
            "styczeń",
            "luty",
            "marzec",
            "kwiecień",
            "maj",
            "czerwiec",
            "lipiec",
            "sierpień",
            "wrzesień",
            "październik",
            "listopad",
            "grudzień"
        };

        private static readonly Regex NonThematicRegex = new Regex(@"\d{4}$", RegexOptions.Compiled);

        public bool IsToBeRun(Options opt) => !string.IsNullOrWhiteSpace(opt.NonNewsCategoriesFill);

        public void DoWork(WikiSite nonsa, Options opt)
        {
            _nonsa = nonsa;
            _dryRun = opt.DryRun;

            if (!int.TryParse(opt.NonNewsCategoriesFill, out int year))
            {
                Console.WriteLine($"Podany argument \"{opt.NonNewsCategoriesFill}\" jest czymś, ale na pewno nie rokiem. Podaj jakąś normalną liczbę, co?");
                return;
            }

            //1. Kategoria:NonNews 2020
            CreateOrSkip($"Kategoria:NonNews {year}", $"[[Kategoria:NonNews|{year}]]");

            //2. kategorie miesięcy
            for (int i = 0; i < Months.Length; i++)
            {
                string content = $"__EXPECTUNUSEDCATEGORY__\n[[Kategoria:NonNews {year}|* {i+1}]]";
                CreateOrSkip($"Kategoria:NonNews – {Months[i]} {year}", content);
            }

            //3. kategorie tematyczne (opcjonalnie)
            if (!int.TryParse(opt.NonNewsCategoriesFillPrev, out int prevYear))
            {
                Console.WriteLine("Nie podano argumentu z poprzednim rokiem, to olewam katy tematyczne, elooo.");
                return;
            }

            var gen = new CategoryMembersGenerator(nonsa)
            {
                CategoryTitle = $"Kategoria:NonNews {prevYear}",
                MemberTypes = CategoryMemberTypes.Subcategory,
                PaginationSize = 500
            };

            gen.EnumItemsAsync().ForEachAsync(prevCat =>
            {
                if (NonThematicRegex.IsMatch(prevCat.Title) || !prevCat.Title.Contains("–")) return;

                var split = prevCat.Title.Split('–');
                var theme = split[1].Trim();

                if (string.IsNullOrEmpty(theme)) return;

                var title = prevCat.Title.Replace(prevYear.ToString(), year.ToString());
                string content = $"[[Kategoria:NonNews {year}|{theme}]]";
                CreateOrSkip(title, content);
            }).Wait();
        }

        private void CreateOrSkip(string title, string content)
        {
            var cat = new WikiPage(_nonsa, title);
            cat.RefreshAsync(PageQueryOptions.FetchContent).Wait();

            if (cat.Exists)
            {
                Console.WriteLine($"{cat.Title} już istnieje, lecim dalej");
            }
            else
            {
                cat.Content = content;
                if (!_dryRun)
                    cat.UpdateContentAsync("", false, true).Wait();
                Console.WriteLine($"Utworzono {title}");
            }
        }
    }
}
