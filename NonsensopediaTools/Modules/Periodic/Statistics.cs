using System;
using System.Text.RegularExpressions;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Periodic
{
    public class Statistics : IModule
    {
        private static readonly Regex StarRegex = new Regex(@"\*\s");
        
        public bool IsToBeRun(Options opt) => opt.Statistics;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            var statPage = new WikiPage(nonsa, "Nonsensopedia:Statystyki dzień po dniu");
            statPage.RefreshAsync(PageQueryOptions.FetchContent).Wait();

            string replacement;
            if (DateTime.Now.DayOfYear == 1)
            {
                replacement = $"{{{{subst:Dzień po dniu}}}}\n\n=== [[{DateTime.Now.Year - 1}]] ===\n* ";
                statPage.Content = statPage.Content.Replace($"[[{DateTime.Now.Year - 1}]]", $"[[{DateTime.Now.Year}]]");
            }
            else if (DateTime.Now.Day == 1)
                replacement = "{{subst:Dzień po dniu}}\n----\n* ";
            else
                replacement = "{{subst:Dzień po dniu}}\n* ";

            statPage.Content = StarRegex.Replace(statPage.Content, replacement, 1);
            
            if (!opt.DryRun)
                statPage.UpdateContentAsync("uzupełniam statystyki", false, false).Wait();
        }
    }
}