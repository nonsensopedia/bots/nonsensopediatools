using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using NonsensopediaTools.Analytics;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Periodic
{
    public class PopularPagesReport : IModule
    {
        public bool IsToBeRun(Options opt) =>
            new []{"long", "short", "both"}.Contains(opt.PopularPagesReport);

        public void DoWork(WikiSite nonsa, Options opt)
        {
            if (opt.PopularPagesReport == "both")
            {
                ReallyDoWork(nonsa, "short", opt.DryRun);
                ReallyDoWork(nonsa, "long", opt.DryRun);
            }
            else ReallyDoWork(nonsa, opt.PopularPagesReport, opt.DryRun);
        }

        private void ReallyDoWork(WikiSite nonsa, string mode, bool dryRun)
        {
            var ga = new AnalyticsInterface();
            var qualified = new List<(PageStats PageStats, WikiPage WikiPage)>();
            var len = mode == "short" ? 7 : 30;
            var startDate = DateTime.Now.AddDays(-len);
            var limit = mode == "short" ? 50 : 100;

            foreach (var pageStat in ga.GetData(startDate))
            {
                var splitTitle = pageStat.PageTitle.Split(" – ");
                if (splitTitle.Length != 2) continue;
                var title = splitTitle[0];

                WikiPage page = new WikiPage(nonsa, title);
                page.RefreshAsync().Wait();
                if (!page.Exists ||
                    page.IsRedirect ||
                    page.NamespaceId == 108 ||
                    page.Title == "Szukaj" ||
                    page.Title.Contains("Strona główna") ||
                    !Constants.ContentNamespaces.Contains(page.NamespaceId))
                {
                    continue;
                }
                
                pageStat.Perturb(new Random());
                qualified.Add((pageStat, page));
                if (qualified.Count >= limit) break;
            }

            var data = qualified
                .OrderBy(x => x.WikiPage.Title)
                .Select(x => MakeSubobjectText(x, len))
                .Aggregate((a, b) => a.Append(b));
            data.Append("\nTo strona zawierająca dane do statystyk odwiedzin. Uzupełnia ją okresowo bot. Nie zmieniaj tu niczego.\n")
                .Append("<onlyinclude>Dane od ").Append(startDate.ToString("yyyy-MM-dd"))
                .Append(" do ").Append(DateTime.Now.ToString("yyyy-MM-dd")).Append(".</onlyinclude>")
                .Append("\n\n[[Kategoria:Statystyki|odwiedzin]]");

            var dataPage = new WikiPage(
                nonsa, 
                "Nonsensopedia:Statystyki odwiedzin/dane_" + mode
            );
            dataPage.RefreshAsync(PageQueryOptions.FetchContent).Wait();
            dataPage.Content = data.ToString();
            
            if (dryRun) return;

            dataPage.UpdateContentAsync("Aktualizuję statystyki odwiedzin", false, true).Wait();
        }

        private static StringBuilder MakeSubobjectText((PageStats stats, WikiPage page) p, int len)
        {
            var sb = new StringBuilder();
            var (stats, page) = p;
            sb.Append("{{#subobject:|Jest obiektem statystyk strony=").Append(page.Title);
            sb.Append("|Ma treść=[[").Append(page.Title).Append("]]");
            sb.Append("|Długość przedziału statystyk=").Append(len);
            sb.Append("|Ma ilość odwiedzin=").Append(stats.Views);
            sb.Append("|Ma ilość unikalnych odwiedzin=").Append(stats.UniqueViews);
            sb.Append("|Ma odsetek wyjść=").Append(
                stats.ExitRate.ToString(Constants.PolishCulture));
            sb.Append("|Średni czas spędzony na stronie=").Append(
                stats.AvgTime.ToString(Constants.PolishCulture));
            sb.Append("}}");
            return sb;
        }
    }
}