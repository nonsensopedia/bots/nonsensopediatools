﻿using System;
using System.IO;

namespace NonsensopediaTools
{
    public static class LoginLoader
    {
        public static (string login, string pass) GetLoginPass(string file)
        {
            if (!File.Exists(file))
            {
                File.Create(file);
                Console.WriteLine($"Nie znalazłem pliku z loginem i hasłem, więc ci taki plik zrobiłem: {file}, powinien być obok binarki.\n" +
                                  $"W pierwszej linijce wpisz login, w drugiej hasło.");
            }
            
            using var f = new StreamReader(file);
            var l = f.ReadLine().Trim();
            var p = f.ReadLine().Trim();

            return (l, p);
        }
    }
}
