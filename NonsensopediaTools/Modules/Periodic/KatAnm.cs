using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Periodic
{
    public class KatAnm : IModule
    {
        private static readonly Regex KatAnmRegex = new Regex(
            @"{{katanm(?>[^\{]*({{medal mały}})?)*[^\{]*}}", 
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase
        );
        
        public bool IsToBeRun(Options opt) => opt.KatAnm;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            int updated = 0, added = 0, removed = 0;
            
            Console.WriteLine("Robię {{KatANM}}...");

            var catExceptions = Utils.GenerateSubcategoriesRecursively(nonsa, "Kategoria:Artykuły potrzebujące redakcji")
                .ToHashSet();
            catExceptions.UnionWith(Utils.GetSubcategories(nonsa, "Kategoria:Ukryte kategorie"));
            catExceptions.UnionWith(Utils.GetSubcategories(nonsa, "Kategoria:Zalążki artykułów"));
            catExceptions.Add("Kategoria:Artykuły na medal");
            catExceptions.Add("Kategoria:Artykuły ze Słoniem jakości");
            catExceptions.Add("Kategoria:Artykuły na śmietnik");
            
            // category title -> (is ANM, page title)
            var dict = new ConcurrentDictionary<string, List<(bool IsAnm, string Title)>>();

            void ProcessPage(WikiPageStub p, bool isAnm)
            {
                var catGen = new CategoriesGenerator(nonsa, p.Title);
                catGen.EnumItemsAsync().ForEachAsync(c =>
                {
                    if (catExceptions.Contains(c.Title)) return;
                    var list = dict.GetOrAdd(c.Title, _ => new List<(bool, string)>());
                    lock (list)
                    {
                        list.Add((isAnm, p.Title));
                    }
                }).Wait();
            }

            var genAnm = new CategoryMembersGenerator(nonsa)
            {
                CategoryTitle = "Kategoria:Artykuły na medal",
                PaginationSize = 5000,
                NamespaceIds = Constants.ContentNamespaces
            };
            genAnm.EnumItemsAsync().ForEachAsync(p => ProcessPage(p, true)).Wait();
            
            var genGood = new CategoryMembersGenerator(nonsa)
            {
                CategoryTitle = "Kategoria:Artykuły ze Słoniem jakości",
                PaginationSize = 5000,
                NamespaceIds = Constants.ContentNamespaces
            };
            genGood.EnumItemsAsync().ForEachAsync(p => ProcessPage(p, false)).Wait();

            foreach (var (key, value) in dict)
            {
                var cat = new WikiPage(nonsa, key);
                var catTask = cat.RefreshAsync(PageQueryOptions.FetchContent);

                var sb = new StringBuilder("{{KatANM|");

                foreach (var p in value)
                {
                    if (p.IsAnm)
                        sb.Append("\n* {{medal mały}} '''[[").Append(p.Title).Append("]]'''");
                    else
                        sb.Append($"\n* '''[[{p.Title}]]'''");
                }

                sb.Append("\n}}");

                catTask.Wait();
                if (!cat.Exists) continue;
                
                if (KatAnmRegex.IsMatch(cat.Content))
                {
                    cat.Content = KatAnmRegex.Replace(cat.Content, sb.ToString());
                    updated++;

                    if (!opt.DryRun) cat.UpdateContentAsync("Zaktualizowano [[Szablon:KatANM]]", true, true).Wait();
                }
                else
                {
                    var ix = cat.Content.LastIndexOf("}}", StringComparison.Ordinal);
                    cat.Content = ix == -1 
                        ? cat.Content.Insert(0, $"{sb}\n\n") 
                        : cat.Content.Insert(ix + 2, $"\n{sb}");

                    added++;
                    if (!opt.DryRun) cat.UpdateContentAsync("Wstawiono [[Szablon:KatANM]]", true, true).Wait();
                }
            }

            var linkingGen = new TranscludedInGenerator(nonsa, "Szablon:KatANM")
            {
                PaginationSize = 5000,
                NamespaceIds = new[] {14}
            };

            linkingGen.EnumPagesAsync(PageQueryOptions.FetchContent)
                .Where(p => !dict.ContainsKey(p.Title))
                .ForEachAsync(cat =>
                    {
                        cat.Content = KatAnmRegex.Replace(cat.Content, "").Trim();
                        removed++;
                        if (!opt.DryRun) cat.UpdateContentAsync("Usunięto zbędny [[Szablon:KatANM]]", true, true).Wait();
                    }
                ).Wait();
            
            Console.WriteLine($"KatANM dodane: {added} zmienione: {updated} usunięte: {removed}");
        }
    }
}
