using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Occasional
{
    public class OdslownieResults : IModule
    {
        public bool IsToBeRun(Options opt) => opt.OdslownieResults != "";

        public void DoWork(WikiSite nonsa, Options opt)
        {
            var param = new Dictionary<string, string>
            {
                { "action", "askargs" },
                { "format", "json" },
                { "api_version", "3" },
                { 
                    "conditions", 
                    $"Identyfikator konkursu::OD{opt.OdslownieResults}|" +
                    $"-Ma podobiekt.Ma podobiekt.Jest zgłoszeniem do Odsłownie {opt.OdslownieResults} strony::+" 
                },
                { "printouts", "-Ma podobiekt|Średnia ocena" },
                { "parameters", "limit=250" }
            };

            var result = Constants.NonsaApiEndpoint
                .SetQueryParams(param)
                .GetStringAsync()
                .Result;
            var json = JObject.Parse(result);

            var pages = new List<(double Score, string Name)>();

            foreach (var r in json["query"]["results"])
            {
                var printouts = (r.First as JProperty).Value["printouts"];
                var pageName = (printouts["-Ma podobiekt"][0]["fulltext"] as JValue).Value<string>();
                var rating = (printouts["Średnia ocena"][0] as JValue).Value<double>();

                pages.Add((rating, pageName));
            }

            int position = 1;
            foreach (var tuple in pages.OrderByDescending(x => x.Score))
            {
                var wikiPage = new WikiPage(nonsa, tuple.Name);
                wikiPage.RefreshAsync(PageQueryOptions.FetchContent).Wait();

                if (wikiPage.Content.Contains("|średnia"))
                {
                    Console.WriteLine($"pomijam {tuple.Name}");
                    position++;
                    continue;
                }

                var sb = new StringBuilder("|średnia=");
                sb.AppendFormat(new CultureInfo("pl-PL"), "{0:F2}", tuple.Score)
                    .AppendLine()
                    .Append("|miejsce=")
                    .Append(position)
                    .AppendLine()
                    .Append("}}");

                wikiPage.Content = wikiPage.Content.Replace("}}", sb.ToString());
                if (!opt.DryRun)
                    wikiPage.UpdateContentAsync("wstawiam wyniki konkursu", false, true).Wait();
                
                Console.WriteLine($"{position}: {tuple.Name}");
                
                position++;
            }
        }
    }
}