﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Archived
{
    class Siostrzane : IModule
    {
        public bool IsToBeRun(Options opt) => opt.Siostrzane;

        private static readonly Regex LinkRegex = new Regex(@"\[\[(.+?)(\|[^\|]+?)?\]\]", RegexOptions.Compiled);

        private static readonly Dictionary<string, string> TemplateNameMap = new Dictionary<string, string>
        {
            { "Cytaty", "Cytaty:" },
            { "Nonnews", "NonNews:" },
            { "Źródła", "Nonźródła:" },
            { "Słownik", "Słownik:" },
            { "Poradnik", "Poradnik:" },
            { "Drogówka odnośnik", "Gra:Drogówka/" },
            { "Pomoc", "Pomoc:" },
            { "Kanciapa", "Nonsensopedia:" }
        };

        public void DoWork(WikiSite nonsa, Options opt)
        {
            const string templateName = "Szablon:Link do siostrzanych";
            const string outputPageName = "Użytkownik:Ostrzyciel/siostrzane";   //for some reason saving to a subpage of Ostrzyciel nożyczek does not work at all

            var namesToCheck = new Dictionary<string, List<(string Template, string Target)>>();
            var templateGen = new TranscludedInGenerator(nonsa)
            {
                NamespaceIds = new [] {10},
                TargetTitle = templateName
            };

            Console.WriteLine("Wyszukuję linki do siostrzanych...");

            //find names to check
            foreach (var template in templateGen.EnumItemsAsync().ToEnumerable())
            {
                if (template.Title == templateName)
                    continue;

                var pageGen = new TranscludedInGenerator(nonsa)
                {
                    NamespaceIds = Constants.ContentNamespaces,
                    TargetTitle = template.Title,
                    PaginationSize = 50     //small pagination size as we are going to grab entire pages
                };
                var tname = template.Title.Split(':')[1];

                pageGen.EnumPagesAsync(PageQueryOptions.FetchContent).ForEachAsync(p =>
                {
                    if (!namesToCheck.TryGetValue(p.Title, out var pageList))
                    {
                        pageList = new List<(string, string)>();
                        namesToCheck.Add(p.Title, pageList);
                    }

                    pageList.AddRange(DoOnePage(p, tname).Select(x => (tname, x)));
                }).Wait();
            }

            //check the names
            Console.WriteLine("Sprawdzam linki do siostrzanych...");
            var invalidLinks = new List<(string From, string Through, string To)>();

            foreach (var kvp in namesToCheck)
            {
                foreach (var link in kvp.Value)
                {
                    var title = new WikiPage(nonsa, link.Target);
                    title.RefreshAsync().Wait();

                    if (title.Exists) continue;
                    
                    invalidLinks.Add((kvp.Key, link.Template, link.Target));
                    Console.WriteLine($"Nieprawidłowy link do siostrzanych z {kvp.Key} przez {link.Template} do {link.Target}");
                }
            }

            if (invalidLinks.Count == 0)
            {
                Console.WriteLine("Brak krzywych linków do siostrzanych.");
                return;
            }

            if (opt.DryRun) return;

            //output results to wiki
            Console.WriteLine("Zwracam wyniki...");
            var output = new WikiPage(nonsa, outputPageName);
            output.RefreshAsync(PageQueryOptions.FetchContent);
            var sb = new StringBuilder(output.Content);
            sb.Append("\n\n== ").Append(DateTime.Now.ToString("dd.MM.yyyy")).Append(" ==\nZnalazłem takie krzywe linki do siostrzanych:\n");

            foreach (var invalidLink in invalidLinks)
            {
                sb.Append("* Ze strony [[").Append(invalidLink.From).Append("]] przez szablon {{S|")
                    .Append(invalidLink.Through).Append("}} do strony [[").Append(invalidLink.To).Append("]]\n");
            }

            sb.Append("~~~~");
            output.Content = sb.ToString().Trim();
            output.UpdateContentAsync(
                $"Znalazłem krzywe linki do siostrzanych, sztuk {invalidLinks.Count}, napraw ktoś bo będę marudzić do znudzenia",
                false, false).Wait();

            Console.WriteLine("Sprawdzono linki do siostrzanych.");
        }

        private IEnumerable<string> DoOnePage(WikiPage page, string tname)
        {
            if (TemplateNameMap.TryGetValue(tname, out var prefix))
            {
                var emptyTemplateRegex = new Regex(@"{{\s*" + tname + @"\s*}}", RegexOptions.IgnoreCase);

                foreach (Match match in emptyTemplateRegex.Matches(page.Content))
                {
                    var m = prefix + (page.Title.Contains(':') ? page.Title.Split(':')[1] : page.Title );
                    yield return m;
                }

                var oneParamTemplateRegex = new Regex(@"{{\s*" + tname + @"\s*\|([^\|\[\]]*?)}}", RegexOptions.IgnoreCase);

                foreach (Match match in oneParamTemplateRegex.Matches(page.Content))
                {
                    var m = prefix + match.Groups[1];
                    yield return m;
                }
            }
            
            var templateRegex = new Regex(@"{{\s*" + tname + @".*?}}", RegexOptions.IgnoreCase);

            foreach (Match match in templateRegex.Matches(page.Content))
            {
                foreach (Match lm in LinkRegex.Matches(match.Value))
                {
                    var name = lm.Groups[1].Value;
                    yield return name;
                }
            }
        }
    }
}
