﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Archived
{
    class NonNewsDateMaker : IModule
    {
        private static readonly Regex TemplateRx = new Regex(@"{{\s*NonNews[ _]data.*?}}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex DateRx = new Regex(@"'''\s*\d{1,2}\s+\w+\s+\d{4}\s*(r\.\s*)?'''\s*$", RegexOptions.Compiled | RegexOptions.Multiline);
        private static readonly Regex WhitespaceRx = new Regex(@"\s+");
        private static readonly Regex WeekRx = new Regex(@"\[\[\s*kategoria:[^\]]*?tydzień[^\]]*?\]\]", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex KatRx = new Regex(@"\[\[\s*kategoria:", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public bool IsToBeRun(Options opt) => opt.NonNewsMakeDate;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            var allNewsGen = new AllPagesGenerator(nonsa)
            {
                NamespaceId = 102,
                PaginationSize = 500,
                RedirectsFilter = PropertyFilterOption.WithoutProperty
            };

            allNewsGen.EnumPagesAsync(PageQueryOptions.FetchContent).ForEachAsync(p =>
            {
                if (p.IsRedirect || TemplateRx.IsMatch(p.Content)) return;

                var date = "";
                var m = DateRx.Match(p.Content);
                if (m.Success)
                {
                    date = m.Value.Replace("'''", "").Replace("r.", "").Trim().ToLowerInvariant();
                    date = WhitespaceRx.Replace(date, " ");
                    Console.WriteLine($"{p.Title} --- z nagłówka --- {date}");
                }
                else
                {
                    var revgen = p.CreateRevisionsGenerator();
                    revgen.TimeAscending = true;
                    revgen.PaginationSize = 1;

                    var rev1 = revgen.EnumItemsAsync().ToArrayAsync().Result[0];
                    date = rev1.TimeStamp.ToString("d MMMM yyyy", new CultureInfo("pl"));
                    Console.WriteLine($"{p.Title} --- z daty utworzenia --- {date}");
                }

                var template = $"{{{{NonNews data|{date}}}}}";
                m = WeekRx.Match(p.Content);
                if (m.Success)
                {
                    p.Content = p.Content.Replace(m.Value, template);
                    Console.WriteLine("podmieniono kat tygodnia");
                }
                else
                {
                    p.Content = KatRx.Replace(p.Content, template + "\n[[Kategoria:");
                    Console.WriteLine("dodano przed katami");
                }

                if (!opt.DryRun)
                    p.UpdateContentAsync("dodano {{NonNews data}}", true, true).Wait();

            }).Wait();
        }
    }
}
