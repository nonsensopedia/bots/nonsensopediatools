using System;
using System.Linq;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Periodic
{
    public class CommonsInformation : IModule
    {
        public bool IsToBeRun(Options opt) => opt.CommonsInformation;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            var allFilesGen = new AllPagesGenerator(nonsa)
            {
                NamespaceId = 6,
                PaginationSize = 1000
            };
            var transcludesInformation = new TranscludedInGenerator(nonsa)
            {
                NamespaceIds = new [] { 6 },
                PaginationSize = 1000,
                TargetTitle = "Szablon:Information"
            };

            var allFilesTask = allFilesGen.EnumItemsAsync().ToArrayAsync();
            var transludesInfoTask = transcludesInformation.EnumItemsAsync().ToArrayAsync();

            var pages = allFilesTask.Result.Except(transludesInfoTask.Result)
                .Select(stub => new WikiPage(nonsa, stub.Id))
                .ToArray();
            pages.RefreshAsync().Wait();

            var missingFiles = pages.Where(x => x.LastFileRevision == null && x.IsRedirect == false);
            foreach (var missingFile in missingFiles)
                Console.WriteLine($"! brak pliku: {missingFile.Title}");

            var pagesToChange = pages
                .Where(x => (x.LastFileRevision?.DescriptionUrl ?? "").StartsWith("https://commons.wikimedia.org"))
                .ToArray();
            pagesToChange.RefreshAsync(PageQueryOptions.FetchContent).Wait();

            foreach (var page in pagesToChange)
            {
                page.Content = "{{Information\n|caption=\n}}\n\n" + page.Content.Trim();
                page.UpdateContentAsync("Wstawiam {{Information}}", true, true).Wait();
            }
            
            Console.WriteLine($"Wstawiono Information na {pagesToChange.Length} stronach");
        }
    }
}