using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Periodic
{
    public class Footer : IModule
    {
        private static readonly Regex CatRegex = new Regex(
            @"^(\[\[\s*(kategoria|category)|\{\{defaultsort):", 
            RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline);
        
        public bool IsToBeRun(Options opt) => opt.Footer;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            Console.WriteLine("Pobieram listę artykułów z brakującym szablonem...");
            var shouldHaveFooter = EnumeratePages().ToHashSet();
            var gen = new TranscludedInGenerator(nonsa, "Szablon:Stopka")
            {
                PaginationSize = 500,
                NamespaceIds = Constants.ContentNamespaces
            };
            var withFooter = gen.EnumItemsAsync().Select(stub => stub.Title).ToEnumerable();
            shouldHaveFooter.ExceptWith(withFooter);

            Console.WriteLine("Wstawiam szablon...");
            foreach (var title in shouldHaveFooter)
            {
                var wikiPage = new WikiPage(nonsa, title);
                wikiPage.RefreshAsync(PageQueryOptions.FetchContent).Wait();

                if (!Constants.ContentNamespaces.Contains(wikiPage.NamespaceId))
                {
                    IncidentReporter.AddMessage($"[[:{title}]] ma mieć stopkę, a nie jest w przestrzeni treści");
                    continue;
                }

                if (wikiPage.NamespaceId == 108)
                {
                    IncidentReporter.AddMessage($"[[:{title}]] jest stroną Gry, a ma niby mieć stopkę.");
                    continue;
                }

                if (wikiPage.Content.Contains("{{stopka}}", StringComparison.OrdinalIgnoreCase))
                {
                    IncidentReporter.AddMessage(
                        $"Coś nie gra: [[:{title}]] niby nie transkluduje stopki, a ma ją w kodzie.");
                    continue;
                }

                var newContent = CatRegex.Replace(
                    wikiPage.Content, 
                    match => "{{stopka}}\n" + match.Value,
                    1
                );

                if (newContent == wikiPage.Content)
                {
                    Console.WriteLine($"Pozor: nie znalaziono kategorii na stronie {title}");
                    newContent = wikiPage.Content + "\n\n{{stopka}}";
                }

                wikiPage.Content = newContent;
                if (!opt.DryRun)
                    wikiPage.UpdateContentAsync("Dodaję szablon {{stopka}}", true, true).Wait();
            }
            
            Console.WriteLine($"Dodano szablon na stronach: {shouldHaveFooter.Count}");
        }

        private IEnumerable<string> EnumeratePages()
        {
            var param = new Dictionary<string, string>
            {
                { "action", "askargs" },
                { "format", "json" },
                { "api_version", "3" },
                { "conditions", "Portal:+|Zawiera artykuł::+" },
                { "printouts", "Zawiera artykuł" },
                { "parameters", "limit=100" }
            };
            
            var result = Constants.NonsaApiEndpoint
                .SetQueryParams(param)
                .GetStringAsync()
                .Result;
            var json = JObject.Parse(result);

            return json["query"]["results"].Values()
                .SelectMany(portalRow =>
                    portalRow.First["printouts"].First.Values().Select(x => (string) x["fulltext"])
                );
        }
    }
}