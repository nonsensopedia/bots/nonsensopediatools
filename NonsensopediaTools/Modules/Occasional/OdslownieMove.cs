using System;
using System.Text;
using System.Text.RegularExpressions;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Occasional
{
    public class OdslownieMove : IModule
    {
        private static readonly Regex TitleRx = new Regex(@"(?<=''').*?(?=''')", RegexOptions.Compiled);
        private static readonly Regex TextRx = new Regex(@"(?<=\|).*?(?=\|)", 
            RegexOptions.Compiled | RegexOptions.Singleline);
        
        public bool IsToBeRun(Options opt) => opt.OdslownieMove;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            Console.WriteLine("Dajesz te hasła:");

            while (true)
            {
                string s = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(s)) break;
                s = s.Trim();
                
                var page = new WikiPage(nonsa, s);
                page.RefreshAsync(PageQueryOptions.FetchContent).Wait();
                var m = TextRx.Match(page.Content);

                if (!m.Success)
                {
                    Console.WriteLine($"{s} – nie widzę tekstu hasła");
                    continue;
                }

                var text = m.Value.Trim();
                m = TitleRx.Match(text);
                
                if (!m.Success)
                {
                    Console.WriteLine($"{s} – nie widzę tytułu");
                    continue;
                }

                var title = m.Value.Trim();
                var sTitle = new WikiPage(nonsa, "Słownik:" + title);
                sTitle.RefreshAsync().Wait();

                if (sTitle.Exists)
                {
                    Console.WriteLine($"{s} – strona docelowa już istnieje, konieczne ręczne scalanie");
                    continue;
                }

                text = TitleRx.Replace(text, $"[[Słownik:{title}|{title}]]");
                var sb = new StringBuilder(text);
                sb.Append("\n\n<noinclude><!-- Nie usuwaj i nie zmieniaj poniższego szablonu, to hasło pochodzi z konkursu Odsłownie -->\n");
                sb.Append(page.Content.Trim());
                sb.Append("\n</noinclude>");
                page.Content = sb.ToString();
                page.UpdateContentAsync("sprzątanie pokonkursowe", true, true).Wait();

                page.MoveAsync(sTitle.Title, "przenoszenie hasła konkursowego do Słownika",
                    PageMovingOptions.NoRedirect).Wait();
            }
        }
    }
}