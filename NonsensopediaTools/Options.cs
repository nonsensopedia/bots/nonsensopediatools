﻿using CommandLine;

namespace NonsensopediaTools
{
    public enum NonNewsArchiveMode
    {
        All,
        OnlyArchive,
        OnlyNewRows
    }

    public class Options
    {
        [Option("dry-run", Default = false, HelpText = "Wykonuje wszystkie operacje, ale nie zapisuje zmian.")]
        public bool DryRun { get; set; }

        [Option("drzewa-link", Default = false, HelpText = "Dodaje szablon {{Drzewa link}} do wymagających tego kategorii.")]
        public bool DrzewaLink { get; set; }
        
        [Option("kat-anm", Default = false, HelpText = "Pilnuje szablonu {{KatANM}}.")]
        public bool KatAnm { get; set; }

        [Option("nonnews-archive", Default = false, HelpText = "Ogarnia archiwum NonNews.")]
        public bool NonNewsArchive { get; set; }

        [Option("na-ignore-date", Default = false, HelpText = "Pomija sprawdzenie dzisiejszej daty przy robieniu archiwum NonNews.")]
        public bool IgnoreDate { get; set; }

        [Option("na-mode", Default = NonNewsArchiveMode.All, HelpText = "Tryb pracy NonNewsArchive. Dostępne opcje: All, OnlyArchive, OnlyNewRows.")]
        public NonNewsArchiveMode NonNewsArchiveMode { get; set; }
        
        [Option("stats", Default = false, HelpText = "Ogarnia statystyki dzień po dniu.")]
        public bool Statistics { get; set; }

        [Option("siostrzane", Default = false, HelpText = "Sprawdza poprawność linków do siostrzanych.")]
        public bool Siostrzane { get; set; }

        [Option("nonnews-kat", Default = "", HelpText = "Wypełnia kategorie NonNews dla podanego roku. Podanie roku jest obowiązkowe, inaczej nic się nie stanie.")]
        public string NonNewsCategoriesFill { get; set; }

        [Option("nonnews-kat-poprz", Default = "", HelpText = "Opcjonalny parametr dla ww. modułu: rok, z którego powinny zostać skopiowane kategorie tematyczne. Jak nic nie podasz, to nic się nie skopiuje.")]
        public string NonNewsCategoriesFillPrev { get; set; }

        [Option("slownik-linki", Default = false, HelpText = "Dodaje automatycznie linki wewnątrz Słownika.")]
        public bool DictionaryLinksFinder { get; set; }

        [Option("link-remover", Default = false, HelpText = "Delinkuje rzeczy według podanej listy.")]
        public bool LinkRemover { get; set; }

        [Option("metadata-location-adder", Default = false, HelpText = "Dodaje parametr location= w plikach na podstawie metadanych EXIF.")]
        public bool MetadataLinkAdder { get; set; }

        [Option("nonnews-make-date", Default = false, HelpText = "Dodaje szablon {{NonNews data}} do niusów.")]
        public bool NonNewsMakeDate { get; set; }
        
        [Option("popular-pages-report", Default = "", HelpText = "Generuje raport popularnych stron. Dostępne opcje: long, short, both.")]
        public string PopularPagesReport { get; set; }
        
        [Option("commons-information", Default = false, HelpText = "Dodaje szablon {{Information}} do plików importowanych z Commons.")]
        public bool CommonsInformation { get; set; }

        [Option("year-redirect", Default = false, HelpText = "Zmienia przekierowania ze stron lat do stron wieków z podziałem na dziesięciolecia")]
        public bool YearRedirectToSection { get; set; }
        
        [Option("footer", Default = false, HelpText = "Dodaje szablon {{stopka}} do artykułów zawartych w portalach")]
        public bool Footer { get; set; }
        
        [Option("odslownie-results", Default = "", HelpText = "Wpisuje w szablon zgłoszeniowy pole średnia= i miejsce= dla podanego roku.")]
        public string OdslownieResults { get; set; }
        
        [Option("odslownie-move", Default = false, HelpText = "Przenosi zgłoszenia w Odsłownie do Słownika. Przyjmuje na wejściu standardowym listę stron do ogarnięcia.")]
        public bool OdslownieMove { get; set; }
        
        [Option("smw-diff", Default = null, HelpText = "Oblicza różnicę zbioru artykułów z dwóch zapytań Semantic MediaWiki. Ten argument przyjmuje pierwszy zestaw warunków.")]
        public string SmwDifference { get; set; }
        
        [Option("smw-diff2", Default = null, HelpText = "Drugi zestaw warunków dla polecenia smw-diff.")]
        public string SmwDifference2 { get; set; }
        
        [Option("gnm-number", Default = false, HelpText = "Wypełnia numery GNM-ów.")]
        public bool GnmNumber { get; set; }
        
        [Option("kat-portal", Default = false, HelpText = "Dodaje szablon {{KatPortal}} do kategorii zawartych w portalach")]
        public bool KatPortal { get; set; }
    }
}
