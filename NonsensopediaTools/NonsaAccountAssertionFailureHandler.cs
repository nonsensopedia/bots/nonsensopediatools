﻿using System;
using System.Threading.Tasks;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools
{ 
    class NonsaAccountAssertionFailureHandler : IAccountAssertionFailureHandler
    {
        /// <inheritdoc />
        public Task<bool> Login(WikiSite site)
        {
            switch (site)
            {
                case null:
                    throw new ArgumentNullException(nameof(site));
                case NonsaWikiSite wikiSite:
                    Console.WriteLine("Wymagane ponowne zalogowanie...");
                    return Relogin(wikiSite);
                default:
                    throw new ArgumentException("Must be a NonsaWikiSite", nameof(site));
            }
        }

        private async Task<bool> Relogin(NonsaWikiSite site)
        {
            try
            {
                await site.LoginAsync();
                Console.WriteLine("Zalogowano.");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }
    }
}
