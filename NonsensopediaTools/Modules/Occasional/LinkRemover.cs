﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Occasional
{
    class LinkRemover : IModule
    {
        public bool IsToBeRun(Options opt) => opt.LinkRemover;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            Console.WriteLine("Wrzucaj linki do usunięcia w takim formacie:");
            Console.WriteLine("<Ze strony>#<Do strony>");
            Console.WriteLine("Jak skończysz to wrzuć pustą linię.");

            int valid = 0, invalid = 0;
            var links = new Dictionary<string, List<string>>();

            while (true)
            {
                var s = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(s)) break;

                var split = s.Split('#', StringSplitOptions.RemoveEmptyEntries);
                if (split.Length != 2)
                {
                    invalid++;
                    continue;
                }

                var key = Utils.NormalizePageTitle(split[0]);
                var val = Utils.NormalizePageTitle(split[1]);
                valid++;

                if (links.TryGetValue(key, out var list))
                    list.Add(val);
                else links.Add(key, new List<string>(new []{ val }));
            }

            Console.WriteLine($"Strony do przejrzenia: {links.Count}, linki: {valid}. Niepoprawne wiersze: {invalid}. Lecimy!");

            foreach (var link in links)
            {
                var page = new WikiPage(nonsa, link.Key);
                var ptask = page.RefreshAsync(PageQueryOptions.FetchContent);
                var regexes = new List<Regex>();

                foreach (var target in link.Value)
                {
                    var sb = new StringBuilder();
                    var letter = target[..1];
                    sb.Append(@"\[\[ *(").Append(letter.ToLowerInvariant()).Append("|").Append(letter).Append(")");
                    sb.Append(Regex.Escape(target)[1..].Replace(@"\ ", @"( |_)")).Append(@" *(\|[^\]\|]*)?\]\]");
                    regexes.Add(new Regex(sb.ToString()));
                }

                int linksRemoved = 0;
                ptask.Wait();
                foreach (var regex in regexes)
                {
                    page.Content = regex.Replace(page.Content, match =>
                    {
                        linksRemoved++;
                        if (match.Groups.Last<Group>().Value.StartsWith("|"))
                            return match.Groups.Last<Group>().Value[1..].Trim();

                        return match.Value
                            .Replace("[[", "").Replace("]]", "")
                            .Trim();
                    });
                }

                if (!opt.DryRun && linksRemoved > 0)
                    page.UpdateContentAsync($"usunięto linki, {linksRemoved} sztuk", true, true).Wait();

                Console.WriteLine($"Usunięte linki: {linksRemoved}, strona: {page.Title}");
            }
        }
    }
}
