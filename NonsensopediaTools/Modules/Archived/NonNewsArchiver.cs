using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Archived
{
    public class NonNewsArchiver : IModule
    {
        private static readonly Regex LinkRegex = new Regex(@"^\[\[[^\|]+\]\]$", RegexOptions.Compiled);
        private static readonly Regex SectionRegex1 = new Regex(@"(?<=1 -->).*(?=<!-- 2)", RegexOptions.Singleline | RegexOptions.Compiled);
        private static readonly Regex SectionRegex2 = new Regex(@"(?<=2 -->).*(?=<noinclude)", RegexOptions.Singleline | RegexOptions.Compiled);
        private static readonly Regex DoubleRowRegex = new Regex(@"\|-\s*\|-", RegexOptions.Singleline | RegexOptions.Compiled);

        private static readonly Random RandomGen = new Random();

        public bool IsToBeRun(Options opt) => opt.NonNewsArchive;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            try
            {
                //propozycje
                var propozycje = new WikiPage(nonsa, "Nonsensopedia:Z archiwum NonNews/propozycje");
                propozycje.RefreshAsync(PageQueryOptions.FetchContent | PageQueryOptions.ResolveRedirects).Wait();

                //wstawienie pustych wierszy na zaś
                int addedRows = 0;
                if (opt.NonNewsArchiveMode != NonNewsArchiveMode.OnlyArchive)
                {
                    DateTime thursday = DateTime.Today.AddDays(1);
                    while (thursday.DayOfWeek != DayOfWeek.Thursday)
                        thursday = thursday.AddDays(1);

                    for (int days = 0; days < 372; days += 7)
                    {
                        var day = thursday.AddDays(days);
                        if (propozycje.Content.Contains(day.ToString("d MMMM yyyy", Constants.PolishCulture)))
                            continue;
                        
                        var newRow = $"|-\n|{day.ToString("d MMMM yyyy", Constants.PolishCulture)}\n|\n|\n|\n|}}";
                        propozycje.Content = propozycje.Content.Replace("|}", newRow);
                        addedRows++;
                    }
                }

                if (opt.NonNewsArchiveMode != NonNewsArchiveMode.OnlyNewRows)
                {
                    var rows = propozycje.Content.Split("|-\n", StringSplitOptions.RemoveEmptyEntries);
                    if (rows.Length < 2) throw new NonsaException("Pusta tabela propozycji :'(");

                    var columns = rows[1]
                        .Split("|", StringSplitOptions.RemoveEmptyEntries)
                        .Select(x => x.Trim())
                        .ToArray();
                    
                    if (columns.Length != 4)
                        throw new NonsaException(
                            "Pierwszy wiersz tabeli propozycji nie zawiera czterech komórek. ''Ordnung muss sein.''");

                    if (!DateTime.TryParseExact(columns[0], "d MMMM yyyy", Constants.PolishCulture, DateTimeStyles.AllowWhiteSpaces,
                        out var dateRe))
                        throw new NonsaException(
                            "Data republikacji w tabeli propozycji jest w złym formacie. Poprawny format to " +
                            DateTime.Today.ToString("d MMMM yyyy", Constants.PolishCulture));

                    if (!opt.IgnoreDate && dateRe != DateTime.Today)
                        throw new NonsaException(
                            "Pierwszy wiersz w tabeli propozycji ma niedzisiejszą datę. Powinno być "
                            + DateTime.Today.ToString("d MMMM yyyy", Constants.PolishCulture)
                            + ", a jest " + dateRe.ToString("d MMMM yyyy", Constants.PolishCulture));

                    if (!DateTime.TryParseExact(columns[1], "d MMMM yyyy", Constants.PolishCulture, DateTimeStyles.AllowWhiteSpaces,
                        out var dateOr))
                        throw new NonsaException(
                            "Data pierwotnej publikacji w tabeli propozycji jest w złym formacie. Poprawny format to " +
                            DateTime.Today.ToString("d MMMM yyyy", Constants.PolishCulture));

                    columns[2] = columns[2].Trim();
                    if (!LinkRegex.IsMatch(columns[2]))
                        throw new NonsaException(
                            "Pierwszy wiersz w tabeli propozycji nie zawiera linku lub link jest niepoprawny. Daj zwykły link do newsa, bez udziwnień.");

                    propozycje.Content = propozycje.Content.Replace(rows[1], "");
                    propozycje.Content = DoubleRowRegex.Replace(propozycje.Content, "|-");

                    //szablon:wiadomości
                    var wiad = new WikiPage(nonsa, "Szablon:Wiadomości/archiwum");
                    wiad.RefreshAsync(PageQueryOptions.FetchContent).Wait();
                    var m1 = SectionRegex1.Match(wiad.Content);
                    if (!m1.Success)
                        throw new NonsaException(
                            "Nie mogę znaleźć znaczników pierwszej sekcji w szablonie wiadomości. Może ktoś przy nich grzebał, hę?");
                    var m2 = SectionRegex2.Match(wiad.Content);
                    if (!m2.Success)
                        throw new NonsaException(
                            "Nie mogę znaleźć znaczników drugiej sekcji w szablonie wiadomości. Może ktoś przy nich grzebał, hę?");

                    var title = columns[2].Substring(2, columns[2].Length - 4);
                    var titleNoNs = title[(title.IndexOf(':') + 1)..];
                    var sb = new StringBuilder("[[").Append(dateOr.ToString("d MMMM", Constants.PolishCulture))
                        .Append($"]] [[{dateOr.Year}]]\n* ").Append($"[[{title}|{titleNoNs}]]");

                    wiad.Content = wiad.Content.Replace(m1.Value.Trim(), sb.ToString());
                    wiad.Content = wiad.Content.Replace(m2.Value.Trim(), m1.Value.Trim());

                    //archiwum
                    var arch = new WikiPage(nonsa, "Nonsensopedia:Z archiwum NonNews/archiwum");
                    arch.RefreshAsync(PageQueryOptions.FetchContent).Wait();

                    var newrow = $"|-\n{rows[1].Trim()}\n";
                    int ix = arch.Content.IndexOf("|}", StringComparison.Ordinal);
                    if (ix < 0) throw new NonsaException("Nie mogę znaleźć końca tabeli w archiwum.");
                    arch.Content = arch.Content.Insert(ix, newrow);

                    if (!opt.DryRun)
                    {
                        Console.WriteLine("Zapisuję zmiany...");

                        //wykonaj zmiany
                        wiad.UpdateContentAsync("Nowy news z archiwum", true, false).Wait();
                        arch.UpdateContentAsync("Uzupełniam archiwum", true, false).Wait();
                        propozycje.UpdateContentAsync(
                            "Archiwalny news poszedł na główną" + 
                            $"{(addedRows > 0 ? $", dodano nowych wierszy: {addedRows}" : "")}",
                            true, 
                            false
                        ).Wait();
                    }
                }
                else if (!opt.DryRun)
                {
                    Console.WriteLine("Zapisuję zmiany...");
                    propozycje.UpdateContentAsync($"Archiwalny news poszedł na główną" +
                                                  $"{(addedRows > 0 ? $", dodano nowych wierszy: {addedRows}" : "")}",
                        true, false).Wait();
                }

                Console.WriteLine("Done.");
            }
            catch (NonsaException nx)
            {
                try
                {
                    var pyskusja = new WikiPage(nonsa, "Dyskusja Nonsensopedia:Z archiwum NonNews/bot");
                    pyskusja.RefreshAsync(PageQueryOptions.FetchContent).Wait();

                    string t = Constants.Teksty[RandomGen.Next(Constants.Teksty.Length)];
                    string text = $"\n\n{t}\n\n{nx.Message} ~~~~\n";
                    pyskusja.Content += text;

                    if (!opt.DryRun)
                        pyskusja.UpdateContentAsync(
                            t[..^1], 
                            false, 
                            false
                        ).Wait();

                    Console.WriteLine("Nie wyszło.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
            }
        }

    }
}
