FROM mcr.microsoft.com/dotnet/sdk:3.1-alpine as build
WORKDIR /source

# Install dependencies
COPY *.sln .
COPY NonsensopediaTools/*.csproj ./NonsensopediaTools/
RUN dotnet restore

# Build application
COPY NonsensopediaTools/. ./NonsensopediaTools/
WORKDIR /source/NonsensopediaTools
RUN dotnet build -c release -o /app

# Make runtime image
FROM mcr.microsoft.com/dotnet/runtime:3.1-alpine

# Alpine images **have no cultures installed** and are set to ignore this by always
# using the invariant culture. This is bad for this bot, as it has to speak Polish.
RUN apk add --no-cache icu-libs
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

WORKDIR /app
COPY --from=build /app ./
ENTRYPOINT ["/bin/ash"]
