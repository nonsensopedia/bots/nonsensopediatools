﻿using System.Threading.Tasks;
using WikiClientLibrary.Client;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools
{
    class NonsaWikiSite : WikiSite
    {
        private readonly string _userName;
        private readonly string _password;

        public NonsaWikiSite(IWikiClient wikiClient, string apiEndpoint, string userName, string password) : base(wikiClient, new SiteOptions(apiEndpoint), userName, password)
        {
            _userName = userName;
            _password = password;
            AccountAssertionFailureHandler = new NonsaAccountAssertionFailureHandler();
        }

        public Task LoginAsync()
        {
            return LoginAsync(_userName, _password);
        }
    }
}
