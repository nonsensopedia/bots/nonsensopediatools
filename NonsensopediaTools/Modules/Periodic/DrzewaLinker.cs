﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using WikiClientLibrary;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Periodic
{
    public class DrzewaLinker : IModule
    {
        private static readonly string[] ValidPrefixes =
        {
            "Grafiki – ",
            "Słownik – ",
            "Poradniki – ",
            "NonNews – ",
            ""
        };

        private static readonly Regex TemplateRegex = new Regex(@"{{drzewa[ _]link.*?}}", 
            RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

        public bool IsToBeRun(Options opt) => opt.DrzewaLink;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            Console.WriteLine("Robię {{Drzewa link}}...");

            // Base category name -> { prefix -> actual category name }
            var dict = new ConcurrentDictionary<string, Dictionary<string, string>>();

            var gen = new AllPagesGenerator(nonsa)
            {
                NamespaceId = 14,
                PaginationSize = 500
            };
            var stubs = gen.EnumItemsAsync().ToArrayAsync().Result;

            // Index categories
            foreach (var stub in stubs)
            {
                var title = stub.Title.Split(':')[1];
                foreach (var prefix in ValidPrefixes)
                    if (title.StartsWith(prefix))
                    {
                        var baseTitle = title[prefix.Length..];
                        baseTitle = baseTitle[..1].ToUpperInvariant() + baseTitle[1..];
                        dict.AddOrUpdate(
                            baseTitle,
                            new Dictionary<string, string> { { prefix, title } },
                            (key, innerDict) =>
                            {
                                innerDict.Add(prefix, title);
                                return innerDict;
                            }
                        );
                        break;
                    }
            }

            Console.WriteLine($"Znalezione kategorie: {stubs.Length}, grupy: {dict.Count}");

            // Add or update templates
            bool AddTemplate(string cat, string[] categories, int retry = 0)
            {
                if (retry > 2)
                {
                    Console.WriteLine("Za dużo prób, poddaję się.");
                    return false;
                }

                var p = new WikiPage(nonsa, $"Kategoria:{cat}");
                p.RefreshAsync(PageQueryOptions.FetchContent).Wait();
                var sb = new StringBuilder("{{Drzewa link");
                foreach (var category in categories)
                    sb.Append("|").Append(category);
                sb.Append("}}");
                var template = sb.ToString();

                if (p.Content.Contains(template)) return false;

                if (p.Content.ToLowerInvariant().Contains("{{drzewa link"))
                    p.Content = TemplateRegex.Replace(p.Content, template);
                else if (p.Content.Trim().StartsWith("{{"))
                    p.Content = $"{template}\n" + p.Content;
                else p.Content = $"{template}\n\n" + p.Content;

                Console.WriteLine($"Próba {retry + 1}, kategoria {cat}");

                if (opt.DryRun) return true;

                try
                {
                    p.UpdateContentAsync("aktualizuję szablon {{Drzewa link}}", false, true).Wait();
                }
                catch (AggregateException ex)
                {
                    if (ex.InnerException is OperationFailedException)
                    {
                        Console.WriteLine($"Nie udało się zapisać strony {cat}, pewnie chodzi o propagację zmian SMW.");
                        Console.WriteLine(ex.Message);
                        p.PurgeAsync(PagePurgeOptions.ForceLinkUpdate).Wait();
                        AddTemplate(cat, categories, retry + 1);
                    }
                    else
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
                
                return true;
            }

            int counter = 0;
            var categoriesWithTemplate = new HashSet<string>();
            foreach (var inner in dict.Values)
            {
                if (inner.Count < 2) continue;
                
                foreach (var (key, value) in inner)
                {
                    categoriesWithTemplate.Add($"Kategoria:{value}");
                    var cats = inner.Where(x => x.Key != key).Select(x => x.Value);
                    if (AddTemplate(value, cats.ToArray())) counter++;
                }
            }

            Console.WriteLine("Dodałem lub zaktualizowałem {{Drzewa link}} razy: " + counter);

            // Remove unnecessary templates
            int deletionCounter = 0;
            var wlhGen = new TranscludedInGenerator(nonsa, "Szablon:Drzewa link")
            {
                NamespaceIds = new[] {14},
                PaginationSize = 500
            };

            foreach (var stub in wlhGen.EnumItemsAsync().ToEnumerable().Where(x => !categoriesWithTemplate.Contains(x.Title)))
            {
                var p = new WikiPage(nonsa, stub.Title);
                p.RefreshAsync(PageQueryOptions.FetchContent).Wait();
                p.Content = TemplateRegex.Replace(p.Content, "").Trim();
                deletionCounter++;

                if (!opt.DryRun) p.UpdateContentAsync("usuwam {{Drzewa link}}", false, true).Wait();
            }

            Console.WriteLine($"Usunąłem szablon razy: {deletionCounter}");
        }
    }
}
