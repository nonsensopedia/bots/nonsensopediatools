﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using WikiClientLibrary.Sites;
using Newtonsoft.Json.Linq;
using WikiClientLibrary.Pages;

namespace NonsensopediaTools.Modules.Archived
{
    class MetadataLocationAdder : IModule
    {
        private static readonly Regex TemplateEndRegex = new Regex(@"^\}\}", RegexOptions.Compiled | RegexOptions.Multiline);

        public bool IsToBeRun(Options opt) => opt.MetadataLinkAdder;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            foreach (var file in EnumerateFiles())
            {
                var page = new WikiPage(nonsa, file.Id);
                page.RefreshAsync(PageQueryOptions.FetchContent).Wait();
                if (page.Content.ToLowerInvariant().Contains("location")) continue;

                int startat = page.Content.ToLowerInvariant().IndexOf(
                    "information", StringComparison.Ordinal
                );
                
                if (startat < 0)
                {
                    Console.WriteLine("Nie mogę wpisać lokalizacji, bo nie ma Information: " + page.Title);
                    continue;
                }

                var newText = TemplateEndRegex.Replace(
                    page.Content, 
                    $"|location={file.Lat.Trim()}, {file.Long.Trim()}\n}}}}", 
                    1, 
                    startat);

                if (newText == page.Content)
                {
                    Console.WriteLine("Nie udało się dodać parametru location dla " + page.Title);
                    continue;
                }

                Console.WriteLine("Dodaję location= do " + page.Title);
                page.Content = newText;

                if (!opt.DryRun)
                    page.UpdateContentAsync("Dodano parametr location= na podstawie EXIF", true, true).Wait();
            }
        }

        private IEnumerable<(int Id, string Lat, string Long)> EnumerateFiles()
        {
            var baseUrl = Constants.NonsaApiEndpoint +
                "?action=query&format=json&prop=imageinfo&generator=allimages&iiprop=metadata&gailimit=500";
            var client = new HttpClient();
            var url = baseUrl;
            var gaicontinue = "";

            do
            {
                url = baseUrl + "&gaicontinue=" + gaicontinue;
                var response = client.GetStringAsync(url).Result;
                var json = JObject.Parse(response);

                try
                {
                    gaicontinue = (string) json["continue"]["gaicontinue"];
                }
                catch
                {
                    gaicontinue = "";
                }
                
                var pages = json["query"]["pages"].Values()
                    .Where(x => (string)x["imagerepository"] == "local" &&
                                x["imageinfo"][0]["metadata"]
                                    .Count(y => (string)y["name"] == "GPSLongitude" || (string)y["name"] == "GPSLatitude") == 2
                                );

                foreach (var page in pages)
                {
                    var meta = page["imageinfo"][0]["metadata"];
                    var lat = (string) meta.First(x => (string) x["name"] == "GPSLatitude")["value"];
                    var lon = (string) meta.First(x => (string) x["name"] == "GPSLongitude")["value"];

                    if (lat == "" || lat == "0" || lon == "" || lon == "0")
                        continue;

                    yield return (
                        (int)page["pageid"],
                        lat,
                        lon
                    );
                }
            } while (gaicontinue != "");
        }
    }
}
