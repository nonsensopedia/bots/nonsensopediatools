﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommandLine;
using WikiClientLibrary;
using WikiClientLibrary.Client;
using NonsensopediaTools.Modules;

namespace NonsensopediaTools
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args).WithParsed(DoWork);
        }

        private static void DoWork(Options opt)
        {
            if (opt.DryRun) Console.WriteLine("Dry run!");
            Console.WriteLine("Łączę...");
            var client = new WikiClient
            {
                ClientUserAgent = "NonsensopediaTools"
            };

            var loginFile = Environment.GetEnvironmentVariable("NTOOLS_CREDENTIALS_FILE") ?? "login.txt";
            (string Login, string Password) credentials;
            try
            {
                credentials = LoginLoader.GetLoginPass(loginFile);
            }
            catch (WikiClientException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            var nonsa = new NonsaWikiSite(client, Constants.NonsaApiEndpoint, credentials.Login, credentials.Password);
            try
            {
                nonsa.Initialization.Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Nie umiem się połączyć z Nonsą :'(\n" + ex.Message);
                return;
            }
            
            Console.WriteLine("Połączono.");

            foreach (var module in GetAllModules())
            {
#if !DEBUG
                try
                {
#endif
                    if (module.IsToBeRun(opt))
                        module.DoWork(nonsa, opt);
#if !DEBUG
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
#endif
            }
            
            IncidentReporter.ReportAll(nonsa);
            Console.WriteLine("Wykonane.");
        }

        private static IEnumerable<IModule> GetAllModules()
        {
            return Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.IsClass && 
                            (t.Namespace?.StartsWith("NonsensopediaTools.Modules") ?? false) && 
                            typeof(IModule).IsAssignableFrom(t))
                .Select(t => (IModule)Activator.CreateInstance(t));
        }
    }

}
