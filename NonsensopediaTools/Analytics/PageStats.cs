using System;

namespace NonsensopediaTools.Analytics
{
    public struct PageStats
    {
        public string PageTitle;
        public int Views;
        public int UniqueViews;
        public float ExitRate;
        public float AvgTime;

        /**
         * Introduces a perturbation in the data, should be around 1%.
         */
        public void Perturb(Random random)
        {
            Views = PerturbVariable(random, Views);
            UniqueViews = PerturbVariable(random, UniqueViews);
            ExitRate = PerturbVariable(random, ExitRate);
            AvgTime = PerturbVariable(random, AvgTime);
        }

        private static int PerturbVariable(Random random, int value)
        {
            var offset = value / 200;
            value += random.Next(-offset, offset);
            if (value < 0) value = 0;
            return value;
        }
        
        private static float PerturbVariable(Random random, float value)
        {
            var offset = value / 200;
            value += (float) random.NextDouble() * 2 * offset - offset;
            if (value < 0) value = 0;
            return value;
        }
    }
}