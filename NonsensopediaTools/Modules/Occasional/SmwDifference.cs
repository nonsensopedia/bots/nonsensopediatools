using System;
using System.Collections.Generic;
using System.Linq;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Occasional
{
    // Nieskategoryzowane pliki:
    //      --smw-diff "Plik:+|Data utworzenia::+" --smw-diff2 "Kategoria:Grafiki według tematu"
    public class SmwDifference : IModule
    {
        private const int PageSize = 500;
        
        public bool IsToBeRun(Options opt) => !(opt.SmwDifference is null);

        public void DoWork(WikiSite nonsa, Options opt)
        {
            if (string.IsNullOrWhiteSpace(opt.SmwDifference) || string.IsNullOrWhiteSpace(opt.SmwDifference2))
            {
                Console.WriteLine("smw-diff: Nie podano jednego z argumentów.");
                return;
            }
            
            var s1 = GetSmwResults(opt.SmwDifference.Trim());
            var s2 = GetSmwResults(opt.SmwDifference2.Trim());
            s1.ExceptWith(s2);

            foreach (var title in s1)
                Console.WriteLine(title);
        }

        private HashSet<string> GetSmwResults(string query)
        {
            var param = new Dictionary<string, string>
            {
                { "action", "askargs" },
                { "format", "json" },
                { "api_version", "3" },
                { "conditions", query },
            };

            var titles = new HashSet<string>();

            for (int offset=0; offset < 15_000; offset += PageSize)
            {
                param["parameters"] = $"limit={PageSize}|offset={offset}";
                
                var result = Constants.NonsaApiEndpoint
                    .SetQueryParams(param)
                    .GetStringAsync()
                    .Result;
                var json = JObject.Parse(result);

                var newTitles = json["query"]["results"].Values()
                    .Select(x => (string) x.First["fulltext"]).ToHashSet();
                titles.UnionWith(newTitles);
                
                if (newTitles.Count < PageSize) break;
            }

            return titles;
        }
    }
}