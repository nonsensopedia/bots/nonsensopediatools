using System;
using System.Text.RegularExpressions;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Archived
{
    public class GnmNumber : IModule
    {
        private static readonly Regex GnmRx = new Regex(@"\{\{\s*gnm\s*}}",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);
        
        public bool IsToBeRun(Options opt) => opt.GnmNumber;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            Console.WriteLine("Podaj pełne tytuły GNM-ów w kolejności:");
            for (int i=10; ; i+=10)
            {
                var s = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(s)) break;
                
                var p = new WikiPage(nonsa, s.Trim());
                p.RefreshAsync(PageQueryOptions.FetchContent | PageQueryOptions.ResolveRedirects).Wait();
                
                var oldContent = p.Content;
                p.Content = GnmRx.Replace(p.Content, "{{GNM|klucz=" + i + "}}");
                if (oldContent == p.Content) continue;
                p.UpdateContentAsync($"dodaję klucz sortowania: {i}", true, true).Wait();
            }
        }
    }
}