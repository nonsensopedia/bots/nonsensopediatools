using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace NonsensopediaTools.Analytics
{
    public class AnalyticsInterface
    {
        private readonly AnalyticsReportingService _service;

        private const string ViewId = "191218205";
        
        public AnalyticsInterface()
        {
            UserCredential credential;
            var fileName = Environment.GetEnvironmentVariable("NTOOLS_GA_SECRET_FILE") ?? "client_secret.json";
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    new[] { AnalyticsReportingService.Scope.AnalyticsReadonly },
                    "user", CancellationToken.None, new FileDataStore("Analytics.Tokens")
                    ).Result;
            }
            
            _service = new AnalyticsReportingService(new BaseClientService.Initializer
            {
                ApplicationName = "Nondane Gra",    // yeah, whatever, can reuse the same access key
                HttpClientInitializer = credential
            });
        }

        public IEnumerable<PageStats> GetData(DateTime startDate)
        {
            var dateRange = new DateRange
            {
                StartDate = startDate.ToString("yyyy-MM-dd"),
                EndDate = "Today"
            };
            
            var views = new Metric
            {
                Expression = "ga:pageviews",
                Alias = "views"
            };

            var uniqueViews = new Metric
            {
                Expression = "ga:uniquePageviews",
                Alias = "uniqueViews"
            };

            var exitRate = new Metric
            {
                Expression = "ga:exitRate",
                Alias = "exitRate"
            };
            
            var avgTimeOnPage = new Metric
            {
                Expression = "ga:avgTimeOnPage",
                Alias = "avgTimeOnPage"
            };

            var dim = new Dimension
            {
                Name = "ga:pageTitle"
            };

            var filter = new DimensionFilter
            {
                CaseSensitive = true,
                DimensionName = "ga:pageTitle",
                Expressions = new [] {" – Nonsensopedia"}
            };
            
            var orderBy = new OrderBy
            {
                FieldName = "ga:pageviews",
                SortOrder = "DESCENDING"
            };

            var request = new ReportRequest
            {
                DateRanges = new [] {dateRange},
                Dimensions = new [] {dim},
                Metrics = new [] {views, uniqueViews, exitRate, avgTimeOnPage},
                DimensionFilterClauses = new []
                {
                    new DimensionFilterClause{Filters = new [] {filter}}
                },
                OrderBys = new [] {orderBy},
                ViewId = ViewId,
                SamplingLevel = "LARGE"
            };
            var getRequest = new GetReportsRequest
            {
                ReportRequests = new [] {request}
            };

            var response = _service.Reports.BatchGet(getRequest).Execute();

            if (response.Reports[0].Data?.Rows == null) yield break;

            foreach (var reportRow in response.Reports[0].Data.Rows)
            {
                var vals = reportRow.Metrics[0].Values;
                yield return new PageStats
                {
                    PageTitle = reportRow.Dimensions[0],
                    Views = int.Parse(vals[0]),
                    UniqueViews = int.Parse(vals[1]),
                    ExitRate = float.Parse(vals[2]),
                    AvgTime = float.Parse(vals[3])
                };
            }
        }
    }
}