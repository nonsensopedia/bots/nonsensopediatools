﻿using System;
using System.Collections.Generic;
using System.Linq;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules.Occasional
{
    class DictionaryLinksFinder : IModule
    {
        public bool IsToBeRun(Options opt) => opt.DictionaryLinksFinder;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            var gen = new AllPagesGenerator(nonsa)
            {
                NamespaceId = 106,
                PaginationSize = 50,
                RedirectsFilter = PropertyFilterOption.WithoutProperty
            };

            var pages = gen.EnumPagesAsync(PageQueryOptions.FetchContent).ToArrayAsync().Result;
            var dict = new Dictionary<string, WikiPage>();

            foreach (var page in pages)
            {
                string key = page.Title[8..];
                key = key[0].ToString().ToLowerInvariant() + key[1..];

                if (key.Length < 2) continue;

                if (dict.ContainsKey(key))
                {
                    throw new NonsaException($"Duplikat klucza strony słownika: {key}");
                }
                
                dict.Add(key, page);
            }

            foreach (var page in pages)
            {
                int changes = 0;
                var words = page.Content.Split(' ');

                int squareC = 0, curlyC = 0;

                string ProcessWord(string w)
                {
                    if (w.Length == 0) return w;

                    //to zliczanie ma na celu unikanie wstawiania linków w linkach i szablonach
                    //i tak, wiem, to nie będzie działać dla wszystkich przypadków
                    //niemniej takich raczej w Słowniku za dużo nie ma (o ile w ogóle jakieś są)
                    squareC += w.Count(c => c == '[');
                    curlyC += w.Count(c => c == '{');

                    if (squareC == 0 && curlyC == 0)
                    {
                        var key = w;
                        key = key[0].ToString().ToLowerInvariant() + key[1..];

                        if (dict.TryGetValue(key, out var pp))
                        {
                            if (pp.Id == page.Id) return w;

                            Console.WriteLine($"Wstawiam link z [[{page.Title}]] do [[{pp.Title}]] przez słowo {w}");
                            changes++;
                            return $"[[{pp.Title}|{w}]]";
                        }
                    }

                    squareC -= w.Count(c => c == ']');
                    curlyC -= w.Count(c => c == '}');
                    return w;
                }

                var processed = words.Select(ProcessWord).ToArray();

                if (opt.DryRun || changes <= 0) continue;
                
                page.Content = processed.Aggregate((a, b) => $"{a} {b}");
                page.UpdateContentAsync($"dodaję linki wewnątrzsłownikowe, sztuk {changes}", true, true)
                    .Wait();
            }
        }
    }
}
