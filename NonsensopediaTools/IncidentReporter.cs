using System;
using System.Collections.Generic;
using System.Text;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools
{
    public static class IncidentReporter
    {
        private static readonly List<string> _messages = new List<string>();
        
        public static void AddMessage(string message)
        {
            _messages.Add(message);
            Console.WriteLine(message);
        }
        
        public static void ReportAll(WikiSite site)
        {
            if (_messages.Count == 0) 
                return;
            
            var page = new WikiPage(site, Constants.MaintainerUserName, Constants.UserTalkNs);
            page.RefreshAsync(PageQueryOptions.FetchContent).Wait();

            var dt = DateTime.Today;
            var sb = new StringBuilder(page.Content)
                .AppendLine().AppendLine()
                .AppendLine($"== Bot {dt.Day}.{dt.Month}.{dt.Year} ==");

            foreach (var message in _messages)
                sb.AppendLine(message).AppendLine();
                
            sb.AppendLine("~~~~");
            page.Content = sb.ToString();
            page.UpdateContentAsync($"{_messages.Count} komunikat(y) od bota", false, false)
                .Wait();
        }
    }
}